//------------------------------------------------------------
//按键信号有效时，拉低片选信号并等待，之后写入读使能指令，再依次写入读地址
//何时会有miso信号：当FPGA向flash将读使能指令和读地址写入完成时，下一个sck时钟周期就会按照spi的模式进行读取数据并回传
//FPGA在sck的上升沿对miso上的数据进行采样，在下降沿对数据进行更新

module spi_flash_rd_ctrl(
    i_clk_50M,
    i_rst_n,
    i_key_valid,
    i_miso,
    o_mosi,
    o_cs_n,
    o_sck,
    tx_data,
    tx_flag
);
//-----------------------------------端口列表-------------------------------------------//
    input   wire        i_clk_50M;              //系统时钟，50M
    input   wire        i_rst_n;                //复位，低有效
    input   wire        i_key_valid;            //有效的按键信号
    input   wire        i_miso;                 //Flash芯片给的串行数据
    
    output  reg         o_mosi;                 //输出控制Flash的信号和数据
    output  reg         o_cs_n;                 //片选信号，低有效
    output  reg         o_sck;                  //串行时钟信号
    output  wire[7:0]   tx_data;                //输出给uart串口发送模块
    output  reg         tx_flag;                //与发送数据同步的有效标志信号

//-----------------------------------变量声明-------------------------------------------//
    reg [4:0]   cnt_32;
    reg [7:0]   cnt_32_ctrl;
    wire        full_cnt32_flag;

    parameter   IDLE = 3'b001,                  //空闲状态
                READ = 3'b010,                  //读Flash状态
                SEND = 3'b100;                  //发送状态

    parameter   READ_EN = 8'b0000_0011;         //flash读使能指令
    parameter   SADDR = 8'b0000_0000,           //扇区地址
                PADDR = 8'b0000_0100,           //页地址
                BADDR = 8'b0010_0101;           //首字节地址

    parameter   MAX_NUM_BYTE = 7'd103;          //从Flash中读取出的最大字节数量
    parameter   CNT_WAIT_MAX = 16'd60_000;      //fifo读使能信号间隔，大于等于uart_tx的比特比特发送间隔
    parameter   MAX_FIFO_DATA = 7'd100;         //写入fifo的最多数据量（和从flash中读出的数据量一致）

    reg [2:0]   state;                          //状态机状态变量
    reg [1:0]   cnt_sck;                        //对系统时钟计数四分频，以得到串行时钟sck
    reg [2:0]   cnt_bit;                        //对字节数据输出为串行比特的计数器，控制读使能和读地址指令的串行写入
    reg         miso_flag;                      //miso数据有效标志信号
    reg [7:0]   data_temp;                      //拼接后的字节数据
    reg         po_flag_reg;                    //miso数据有效标志信号
    reg [7:0]   po_data;                        //串行比特拼接成的字节数据
    reg         po_flag;                        //将po_flag_reg打拍和po_data同步
    reg [6:0]   num_fifo_data;                  //对写入fifo的数据数量进行统计
    wire[6:0]   fifo_usedw;                     //fifo中的数据量
    reg [15:0]  cnt_wait;                       //fifo的读使能间隔时间计数器
    reg         fifo_rden;                      //fifo读使能信号
    reg         fifo_rd_valid;                  //fifo操作的控制信号

//-----------------------------读使能指令和读地址的写入-----------------------------------//

    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_32 <= 5'd0;
        else if(cnt_32 == 5'd31)
            cnt_32 <= 5'd0;
        else if(state == READ)
            cnt_32 <= cnt_32 + 5'd1;
        else 
            cnt_32 <= 5'd0;
    end 

    assign  full_cnt32_flag = (cnt_32 == 5'd31);

    //cnt_32计数周期的计数器
    //当key有效时，先写入读使能有效指令，再写入地址信息（三个字节），之后跳转发送状态
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_32_ctrl <= 8'd0;
        else if((cnt_32_ctrl == MAX_NUM_BYTE)&&(full_cnt32_flag == 1'b1))
            cnt_32_ctrl <= 8'd0;
        else if(full_cnt32_flag == 1'b1)
            cnt_32_ctrl <= cnt_32_ctrl + 8'd1;
        else
            cnt_32_ctrl <= cnt_32_ctrl;
    end

    //状态机状态控制
    //当flash的数据全部读取完时，结束READ状态，进入SEND状态
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            state <= IDLE;
        else begin 
            case(state)
                IDLE : state <= (i_key_valid == 1'b1) ? READ : state;
                READ : state <= ((cnt_32_ctrl == MAX_NUM_BYTE)&&(full_cnt32_flag == 1'b1)) ? SEND : state;
                SEND : state <= ((num_fifo_data == MAX_FIFO_DATA)&&(cnt_wait == CNT_WAIT_MAX - 1'b1)) ? IDLE : state;
                default : state <= IDLE;
            endcase
        end
    end

    //sck计数器
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_sck <= 2'd0;
        else if(state == READ)
            cnt_sck <= cnt_sck + 2'd1;
        else
            cnt_sck <= cnt_sck;
    end
    
    //产生sck信号，在读状态
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_sck <= 1'b0;
        else if(state == IDLE)
            o_sck <= 1'b0;              //空闲状态时为低
        else if((cnt_sck == 2'd0)&&(state != IDLE))
            o_sck <= 1'b0;
        else if((cnt_sck == 2'd2)&&(state != IDLE))
            o_sck <= 1'b1;
        else
            o_sck <= o_sck;
    end

    //比特计数器，用于指令的串行比特输出控制
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_bit <= 3'd0;
        else if(state == IDLE)
            cnt_bit <= 3'd0;
        else if((cnt_bit == 3'd7)&&(cnt_sck == 2'd2))
            cnt_bit <= 3'd0;
        else if(cnt_sck == 2'd2)
            cnt_bit <= cnt_bit + 3'd1;
        else
            cnt_bit <= cnt_bit;
    end

    //片选信号控制
    always @(posedge i_clk_50M or negedge i_rst_n) begin
        if(i_rst_n == 1'b0)
            o_cs_n <= 1'b1;                 //片选信号，低有效，默认为低
        else if(i_key_valid == 1'b1)
            o_cs_n <= 1'b0;
        else if((cnt_32_ctrl == MAX_NUM_BYTE)&&(full_cnt32_flag == 1'b1)&&(state == READ))
            o_cs_n <= 1'b1;                 //当读操作完成时，取消片选
        else
            o_cs_n <= o_cs_n;
    end

    //串行控制指令输出，写入读使能指令和读地址
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_mosi <= 1'b0;
        else if((state == READ)&&(cnt_32_ctrl >= 8'd4))
            o_mosi <= 1'b0;                 //写指令和地址写入完成后，不再输出mosi信号，但sck信号依然还有
        else if((state == READ)&&(cnt_sck == 2'd0)&&(cnt_32_ctrl == 8'd0))
            o_mosi <= READ_EN[7-cnt_bit];
        else if((state == READ)&&(cnt_sck == 2'd0)&&(cnt_32_ctrl == 8'd1))
            o_mosi <= SADDR[7-cnt_bit];
        else if((state == READ)&&(cnt_sck == 2'd0)&&(cnt_32_ctrl == 8'd2))
            o_mosi <= PADDR[7-cnt_bit];
        else if((state == READ)&&(cnt_sck == 2'd0)&&(cnt_32_ctrl == 8'd3))
            o_mosi <= BADDR[7-cnt_bit];
        else
            o_mosi <= o_mosi;
    end

//-----------------------------接收来自flash的串行数据-----------------------------------//
    
    //由于给flash传输的sck时钟频率是12.5M，因此读出的数据速率也是12.5M
    //另外，由于SPI采用模式0，因此mosi在sck时钟的上升沿采样、下降沿更新数据
    //同样，miso也是在sck的上升沿采样，在下降沿更新数据
    //所以在设置miso_flag时，应保证flag信号所对应的数据与sck的上升沿对应
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            miso_flag <= 1'b0;
        else if((cnt_sck == 2'd1)&&(state == READ)&&(cnt_32_ctrl >= 8'd4))
            miso_flag <= 1'b1;
        else
            miso_flag <= 1'b0;
    end

    //FPGA在sck的上升沿对miso信号线上的数据进行采样
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            data_temp <= 8'b0;
        else if(miso_flag == 1'b1)
            data_temp <= {data_temp[6:0],i_miso};
        else    
            data_temp <= data_temp;
    end

    //当cnt_bit计数满足7（即足够一个字节）时产生一个字节标志信号，并将此时拼接的字节数据保存给寄存器po_data
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            po_flag_reg <= 1'b0;
        else if((cnt_bit == 3'd7)&&(miso_flag == 1'b1))
            po_flag_reg <= 1'b1;
        else
            po_flag_reg <= 1'b0;
    end

    //当flag信号有效时，将data_temp中拼接的数据保存到po_data中
    //时序上是来得及的，因为在下一个字节的miso数据到来之前，data_temp还是上一个字节的数据，并未变化
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            po_data <= 8'd0;
        else if(po_flag_reg == 1'b1)
            po_data <= data_temp;
        else
            po_data <= po_data;
    end

    //将po_flag_reg打一拍以确保和po_data同步
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            po_flag <= 1'b0;
        else
            po_flag <= po_flag_reg;
    end

    //对写入fifo的数据进行计数
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            num_fifo_data <= 7'd0;
        else if((num_fifo_data == MAX_FIFO_DATA)&&(cnt_wait == CNT_WAIT_MAX))
            num_fifo_data <= 7'd0;
        else if(fifo_rden == 1'b1)
            num_fifo_data <= num_fifo_data + 7'd1;              //是对fifo读使能信号进行计数
        else
            num_fifo_data <= num_fifo_data;
    end
    
//-------------------------------数据写入fifo和读出--------------------------------------//
    
    //fifo操作控制信号
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            fifo_rd_valid <= 1'b0;
        else if(fifo_usedw == MAX_FIFO_DATA)
            fifo_rd_valid <= 1'b1;
        else if((num_fifo_data == MAX_FIFO_DATA)&&(cnt_wait == CNT_WAIT_MAX - 1'b1))
            fifo_rd_valid <= 1'b0;
        else
            fifo_rd_valid <= fifo_rd_valid;
    end


    //实例化调用fifo模块，将拼接的数据字节写入fifo并计数
    fifo_8x128 U_fifo_8x128(
	    .clock  (i_clk_50M),
	    .data   (po_data),
	    .rdreq  (fifo_rden),
	    .wrreq  (po_flag),

	    .q      (tx_data),
	    .usedw  (fifo_usedw)
    );

    //fifo读数间隔，tx发送两个比特的间隔是5208个clk,加上数据协议的起始位和停止位共计10bit，需要5208*10=52080个clk
    //即每发送完一帧数据需要52080个clk；因此fifo读使能之间的时间间隔至少要为这么长时间
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_wait <= 16'd0;
        else if(cnt_wait == CNT_WAIT_MAX)
            cnt_wait <= 16'd0;
        else if(fifo_rd_valid == 1'b1)
            cnt_wait <= cnt_wait + 16'd1;
        else
            cnt_wait <= 16'd0;
    end

    //产生fifo读使能信号，要求该使能信号的间隔要大于等于uart_tx的数据发送间隔
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            fifo_rden <= 1'b0;
        else if((cnt_wait == CNT_WAIT_MAX - 1'b1)&&(fifo_rd_valid == 1'b1)&&(fifo_usedw > 0))
            fifo_rden <= 1'b1;
        else
            fifo_rden <= 1'b0;
    end

    //由于fifo读出数据需要一拍时间，因此tx_data会比fifo_rden晚一拍，所以将fifo_rden再打一拍和tx_data同步
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            tx_flag <= 1'b0;
        else
            tx_flag <= fifo_rden;
    end

endmodule