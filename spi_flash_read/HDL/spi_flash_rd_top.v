module spi_flash_rd_top(
    i_clk_50M,
    i_rst_n,
    i_key,
    i_miso,
    o_mosi,
    o_cs_n,
    o_sck,
    o_tx
);
    input   wire    i_clk_50M;
    input   wire    i_rst_n;
    input   wire    i_key;
    input   wire    i_miso;

    output  wire    o_mosi;
    output  wire    o_cs_n;
    output  wire    o_sck;
    output  wire    o_tx;

    parameter   CNT_MAX = 20'd999_999;
    parameter   CLK_FREQ = 'd50_000_000, UART_BPS = 'd9600;

    wire        key_flag;
    wire[7:0]   tx_data;
    wire        tx_flag;

    //按键消抖
    key_filter
    #(
        .CNT_MAX(CNT_MAX)               //计数器计数最大值
    )
    U_key_filter
    (
        . sys_clk   (i_clk_50M),        //系统时钟50Mhz
        . sys_rst_n (i_rst_n),          //全局复位
        . key_in    (i_key),            //按键输入信号

        . key_flag  (key_flag)          //key_flag为1时表示消抖后检测到按键被按下,key_flag为0时表示没有检测到按键被按下                           
    );

    //flash读
    spi_flash_rd_ctrl U_SPI_flash_rd(
        .i_clk_50M  (i_clk_50M),
        .i_rst_n    (i_rst_n),
        .i_key_valid(key_flag),
        .i_miso     (i_miso),

        .o_mosi     (o_mosi),
        .o_cs_n     (o_cs_n),
        .o_sck      (o_sck),
        .tx_data    (tx_data),
        .tx_flag    (tx_flag)
    );

    //实例化uart发送模块
    uart_tx
    #(
        .UART_BPS(UART_BPS),                //串口波特率
        .CLK_FREQ(CLK_FREQ)                 //时钟频率
    )
    U_uart_tx
    (
        .sys_clk     (i_clk_50M),           //系统时钟50MHz
        .sys_rst_n   (i_rst_n),             //全局复位
        .pi_data     (tx_data),             //模块输入的8bit数据
        .pi_flag     (tx_flag),             //并行数据有效标志信号

        .tx          (o_tx)                 //串转并后的1bit数据
    );


endmodule