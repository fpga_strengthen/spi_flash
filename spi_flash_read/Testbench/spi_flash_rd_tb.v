`timescale 1ns/1ps

module spi_flash_rd_tb();

    reg     clk_50M;
    reg     rst_n;
    reg     key;
    
    wire    miso;
    wire    mosi;
    wire    cs_n;
    wire    sck;
    wire    tx;

    //时钟和复位初始化
    initial begin
        clk_50M = 1'b0;
        rst_n <= 1'b0;
        key <= 1'b0;
        #100
        rst_n <= 1'b1;
        key <= 1'b1;
        #20
        key <= 1'b0;
    end

    always #10 clk_50M = ~clk_50M;              //系统时钟，50M

    //参数重定义，缩短仿真时间
    defparam    memory.mem_access.initfile = "initM25P16_test.txt";
    defparam    U_spi_flash_rd_top.U_SPI_flash_rd.CNT_WAIT_MAX = 1000;
    defparam    U_spi_flash_rd_top.U_uart_tx.CLK_FREQ = 100000;

    //顶层模块实例化调用
    spi_flash_rd_top U_spi_flash_rd_top(
        .i_clk_50M  (clk_50M),
        .i_rst_n    (rst_n),
        .i_key      (key),
        .i_miso     (miso),

        .o_mosi     (mosi),
        .o_cs_n     (cs_n),
        .o_sck      (sck),
        .o_tx       (tx)
    );

    //--------------------memory----------------//
    m25p16 memory(
        .c       (sck),
        .data_in (mosi),
        .s       (cs_n),

        .w       (1'b1),
        .hold    (1'b1),
        .data_out(miso)
    );
endmodule