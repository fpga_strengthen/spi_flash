`timescale 1ns/1ps
module spi_flash_be_tb();

    reg     clk_50M;
    reg     rst_n;
    reg     key;

    wire    sck;
    wire    mosi;
    wire    cs_n;

    initial begin
        clk_50M = 1'b0;
        rst_n <= 1'b0;
        key <= 1'b0;
        #40
        rst_n <= 1'b1;
        #200
        key <= 1'b1;
        #20
        key <= 1'b0;
    end

    always #10 clk_50M = ~clk_50M;          //模拟时钟,50MHz

    //写入Flash仿真模型初始值(全F)
    defparam memory.mem_access.initfile = "initmemory.txt";

    //实例化设计模块
    spi_flash_be U_spi_flash_be(
        .i_clk_50M  (clk_50M),
        .i_rst_n    (rst_n),
        .i_key      (key),

        .o_sck      (sck),
        .o_mosi     (mosi),
        .o_cs_n     (cs_n)
    );

    //------------- memory -------------
    m25p16  memory
    (
        .c          (sck    ),  //输入串行时钟,频率12.5Mhz,1bit
        .data_in    (mosi   ),  //输入串行指令或数据,1bit
        .s          (cs_n   ),  //输入片选信号,1bit
        .w          (1'b1   ),  //输入写保护信号,低有效,1bit
        .hold       (1'b1   ),  //输入hold信号,低有效,1bit

        .data_out   (       )   //输出串行数据
    );

endmodule