`timescale  1ns/1ns
module spi_flash_be_top(
    i_clk_50M,
    i_rst_n,
    i_key_touch,
    sck,
    mosi,
    cs_n
);
    input   wire    i_clk_50M;
    input   wire    i_rst_n;
    input   wire    i_key_touch;
    
    output  wire    sck;
    output  wire    mosi;
    output  wire    cs_n;

    //---------------参量和连线声明---------------//
    parameter   CNT_MAX = 20'd999_999;
    wire        flag_key;

    //按键消抖
    key_filter 
    #(
        .CNT_MAX(CNT_MAX)                   //计数器计数最大值
    )
    U_key_filter
    (
        .sys_clk    (i_clk_50M),            //系统时钟50Mhz
        .sys_rst_n  (i_rst_n),              //全局复位
        .key_in     (i_key_touch),          //按键输入信号
 
        .key_flag   (flag_key)              //key_flag为1时表示消抖后检测到按键被按下,key_flag为0时表示没有检测到按键被按下
    );

    //Flash全擦除
    spi_flash_be U_SPI_Flash_Be(
        .i_clk_50M  (i_clk_50M),
        .i_rst_n    (i_rst_n),
        .i_key      (flag_key),

        .o_sck      (sck),
        .o_mosi     (mosi),
        .o_cs_n     (cs_n)
    );
                                

endmodule