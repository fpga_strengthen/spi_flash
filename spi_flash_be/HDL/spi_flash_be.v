`timescale  1ns/1ns
module spi_flash_be(
    i_clk_50M,
    i_rst_n,
    i_key,
    o_sck,
    o_mosi,
    o_cs_n
);
    input   wire    i_clk_50M;                  //系统时钟，50M (20ns)
    input   wire    i_rst_n;                    //复位，低有效
    input   wire    i_key;                      //输入的按键信号，是一个脉冲
    output  reg     o_sck;                      //输出数据的串行时钟信号
    output  reg     o_mosi;                     //串行输出的比特数据
    output  reg     o_cs_n;                     //片选信号，低有效

    //按键信号有效时，开始各个信号的计时
    //按键信号有效时，拉低片选信号并等待时间t_SLCH>=5ns；之后开始写入写使能信号对Flash信号锁存，这需要一段时间；
    //锁存信号写入后，等待时间t_CHSH>=5ns；然后片选信号拉高并等待一段时间t_SHSL>=100ns；
    //等待时间阶数后，将片选信号再次拉低并等待t_SLCH>=5ns；之后写入BE指令,这需要一段时间；
    //写入完成后再等待t_CHSH>=5ns，之后片选信号再拉高，完成一个写入全擦除指令的流程。
    //以上等待时间均按照640ns来设置，因为有下限没有上限
    
    //状态机状态声明，共4个状态
    parameter   IDLE = 3'd0,                //空闲状态
                WR_EN = 3'd1,               //锁存状态，写入写使能信号
                DELAY = 3'd2,               //延时状态
                BE_WR = 3'd3;               //全擦除信号写入状态

    reg [2:0]   state;                      //状态机状态信号

    //写入指令,分别是写锁存指令和全擦除指令
    parameter   LOCKED_Instru = 8'b0000_0110, BE_Instru = 8'b1100_0111;

    //片选信号控制
    //在拉低cs信号时，cnt=0，此时跳转到WR_EN状态；等待640ns后，cnt=1，开始写入写使能信号；
    //等待640ns，写使能信号写完后，cnt=2；开始等待640ns，等待结束后，cnt=3；
    //拉高cs信号(至少100ns)，等待640ns，cnt=4；在写入BE信号前，须先等待640ns，等待结束时cnt=5；
    //等待结束后，开始写入BE指令，需要640ns，写入完成后，cnt=6；再等待640ns，将cs信号拉高，结束。

    reg [4:0]   cnt_32;               //计数640ns，32个系统时钟周期
    reg [2:0]   cnt_32_ctrl;

    //系统时钟的计数器，sck的频率要求低于20MHz，对系统时钟做四分频得到12.5MHz
    reg [1:0]   cnt_sck;

    wire    wr_lock_flag; 
    wire    wr_be_flag;
    
    wire        full_cnt_32;
    assign      full_cnt_32 = (cnt_32 == 5'd31);
    
    //控制MOSI输出
    reg [2:0]   cnt_bit;

    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_cs_n <= 1'b1;                     //低有效，复位为高
        else if(i_key == 1'b1)
            o_cs_n <= 1'b0;                     //检测到按键信号就拉低
        else if((cnt_32_ctrl == 3'd2) && (full_cnt_32 == 1'b1))
            o_cs_n <= 1'b1;                     //写入完成  
        else if((cnt_32_ctrl == 3'd3) && (full_cnt_32 == 1'b1))
            o_cs_n <= 1'b0;                     //写使能信号写入完成
        else if((cnt_32_ctrl == 3'd6) && (full_cnt_32 == 1'b1))
            o_cs_n <= 1'b1;                     //写入BE信号前拉低cs信号开始等待
        else
            o_cs_n <= o_cs_n;
    end

    always @(posedge i_clk_50M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)
            cnt_32 <= 5'd0;
        else if(cnt_32 == 5'd31)
            cnt_32 <= 5'd0;
        else if(state != IDLE)
            cnt_32 <= cnt_32 + 5'd1;            //在非写入状态时状态机为空闲状态，计数器不需要计数
        else
            cnt_32 <= cnt_32;
    end

    //对延时控制的计数信号做计数，实现状态机的状态跳转控制

    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_32_ctrl <= 3'd0;
        else if( (cnt_32_ctrl == 3'd6) && (cnt_32 == 5'd31))
            cnt_32_ctrl <= 3'd0;
        else if(cnt_32 == 5'd31)
            cnt_32_ctrl <= cnt_32_ctrl + 3'd1;
        else
            cnt_32_ctrl <= cnt_32_ctrl;  
    end

    //状态机状态跳转控制
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            state <= 3'd0;
        else begin 
            case(state)
                IDLE : begin 
                    if(i_key == 1'b1)
                        state <= WR_EN;
                    else
                        state <= state;
                end
                WR_EN : begin 
                    if((cnt_32_ctrl == 3'd2) && (full_cnt_32 == 1'b1))
                        state <= DELAY;
                    else
                        state <= state;
                end
                DELAY : begin 
                    if((cnt_32_ctrl == 3'd3) && (full_cnt_32 == 1'b1))
                        state <= BE_WR;
                    else
                        state <= state;
                end
                BE_WR : begin 
                    if((cnt_32_ctrl == 3'd6) && (full_cnt_32 == 1'b1))
                        state <= IDLE;
                    else
                        state <= state;
                end
                default: state <= IDLE;
            endcase
        end
    end

    assign  wr_lock_flag = (cnt_32_ctrl == 3'd1);
    assign  wr_be_flag   = (cnt_32_ctrl == 3'd5);

    //只在数据写入时产生sck信号
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_sck <= 2'd0;
        else if(cnt_sck == 2'd3)
            cnt_sck <= 2'd0;
        else if(wr_lock_flag == 1'b1)           //因为cnt_32_ctrl 是在WR_EN状态内包含的，所以不需要加state的判断
            cnt_sck <= cnt_sck + 2'd1;
        else if(wr_be_flag == 1'b1)
            cnt_sck <= cnt_sck + 2'd1;
        else
            cnt_sck <= 2'd0;                    //在其他时候不需要做分频信号处理
    end

    //输出数据的sck串行时钟信号
    //SPI为模式0，CPOL=0，sck信号在空闲状态下为低电平；在sck信号的上升沿对数据采样，在下降沿对数据更新
    //因为空闲状态下，cnt_sck一直为0，所以应该在一进入锁存状态就对数据采样
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_sck <= 1'b0;
        else if((cnt_sck == 2'd0)&&(wr_lock_flag == 1'b1))
            o_sck <= 1'b0;
        else if((cnt_sck == 2'd2)&&(wr_lock_flag == 1'b1))
            o_sck <= 1'b1;
        else if((cnt_sck == 2'd0)&&(wr_be_flag== 1'b1))
            o_sck <= 1'b0;
        else if((cnt_sck == 2'd2)&&(wr_be_flag== 1'b1))
            o_sck <= 1'b1;
        else
            o_sck <= o_sck;
    end

    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_bit <= 3'd0;
        else if(cnt_sck == 2'd2)
            cnt_bit <= cnt_bit + 3'd1;
        else
            cnt_bit <= cnt_bit;
    end

    //只需要关注cnt_32_ctrl=2和6时mosi清零时因为它们的上一个状态都对mosi进行了赋值，而其他时候都是为0的
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_mosi <= 1'b0;
        else if((state == WR_EN)&&(cnt_32_ctrl == 3'd2))            
            o_mosi <= 1'b0;
        else if((state == BE_WR)&&(cnt_32_ctrl == 3'd6))
            o_mosi <= 1'b0;
        else if((state == WR_EN)&&(cnt_32_ctrl == 3'd1)&&(cnt_sck == 2'd0)) 
            o_mosi <= LOCKED_Instru[7-cnt_bit];
        else if((state == BE_WR)&&(cnt_32_ctrl == 3'd5)&&(cnt_sck == 2'd0))
            o_mosi <= BE_Instru[7-cnt_bit];
        else
            o_mosi <= o_mosi;
    end

endmodule