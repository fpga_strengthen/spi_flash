module spi_flash_seq_top(
    i_clk_50M,
    i_rst_n,
    i_data_bit,
    o_cs_n,
    o_mosi,
    o_sck,
    o_data_bit
);
    input   wire    i_clk_50M;
    input   wire    i_rst_n;
    input   wire    i_data_bit;
    
    output  wire    o_cs_n;
    output  wire    o_mosi;
    output  wire    o_sck;
    output  wire    o_data_bit;

    
//-----------------------模块连线------------------------//
    wire    [7:0]   uart_byte;
    wire            uart_byte_flag;

//-----------------------参数定义------------------------//
    //串口波特率和时钟频率
    parameter   UART_BPS = 'd9600, CLK_FREQ = 'd50_000_000;

//--------------------模块实例化调用----------------------//
    
    //UART串口接收模块
    uart_rx
    #(
        .UART_BPS(UART_BPS),                    //串口波特率
        .CLK_FREQ(CLK_FREQ)                     //时钟频率
    )
    U_uart_rx
    (
        .sys_clk  (i_clk_50M),                  //系统时钟50MHz
        .sys_rst_n(i_rst_n),                    //全局复位
        .rx       (i_data_bit),                 //串口接收数据

        .po_data  (uart_byte),                  //串转并后的8bit数据
        .po_flag  (uart_byte_flag)              //串转并后的数据有效标志信号
    );

    //数据写入flash模块
    spi_flash_seq U_spi_flash_seq(
        .i_clk_50M  (i_clk_50M),
        .i_rst_n    (i_rst_n),
        .pi_flag    (uart_byte_flag),
        .pi_data    (uart_byte),

        .o_cs_n     (o_cs_n),
        .o_mosi     (o_mosi),
        .o_sck      (o_sck)
    );

    //UART串口发送模块
    uart_tx
    #(
        .UART_BPS(UART_BPS),                    //串口波特率
        .CLK_FREQ(CLK_FREQ)                     //时钟频率
    )
    U_uart_tx
    (
        .sys_clk   (i_clk_50M) ,                //系统时钟50MHz
        .sys_rst_n (i_rst_n) ,                  //全局复位
        .pi_data   (uart_byte) ,                //模块输入的8bit数据
        .pi_flag   (uart_byte_flag) ,           //并行数据有效标志信号
    
        .tx        (o_data_bit)                 //串转并后的1bit数据
    );


endmodule