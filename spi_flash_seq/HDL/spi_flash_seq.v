//连续写操作实验：方法简单但是比较慢
//具体思路：等待和延时与之前相同，先写入写锁存指令，之后延时，再写入页写指令、写入地址，写入一个数据
//此时一个数据写入完成，回到IDLE状态。待下次要写入时再重复以上步骤，注意地址要更新。

module spi_flash_seq(
    i_clk_50M,
    i_rst_n,
    pi_flag,
    pi_data,
    o_cs_n,
    o_mosi,
    o_sck
);
    input   wire        i_clk_50M;          //系统时钟，50MHz
    input   wire        i_rst_n;            //复位，低有效
    input   wire        pi_flag;            //输入按键信号，是一个窄脉冲
    input   wire [7:0]  pi_data;            //写入flash的数据

    output  reg         o_cs_n;             //片选信号，低有效
    output  reg         o_mosi;             //输出串行输出信号
    output  reg         o_sck;              //mosi的时钟，在上升沿对mosi进行采样，在下降沿对mosi更新

    //--------------------控制指令和寄存器变量及连线声明----------------------------//

    parameter   IDLE    = 4'b0001,                  //空闲状态
                WR_EN   = 4'b0010,                  //写锁存
                DELAY   = 4'b0100,                  //延时状态
                WR_PP   = 4'b1000;                  //页写状态

    parameter   WEL   = 8'b0000_0110,               //写锁存指令
                PP    = 8'b0000_0010,               //页写控制指令
                //将扇区地址、页地址和字节首地址写在一起，作为写入的初始地址，之后在此地址上累加
                //SADDR = 8'b0000_0000,           //扇区地址
                //PADDR = 8'b0000_0100;           //页地址
                //BADDR = 8'b0010_0101;           //字节首地址
                INIT_ADDR = 24'h00_04_25;           //数据写入的初始地址

    //由每次一个完整的数据写流程结束所需要的cnt_32的周期数决定，最大应为10，可以参考Visio的时序图
    parameter   NUM_CNT_32 = 7'd10;             //cnt_32_ctrl要计数的总数
    
    reg [7:0]   data_num;                       //对输入的数据进行计数
    reg [4:0]   cnt_32;                         //对系统时钟计数32个，共计640ns
    wire        full_cnt_32_flag;               //每当cnt_32计满时拉高该信号一次
    reg [3:0]   state;                          //状态机状态变量
    reg [7:0]   cnt_32_ctrl;                    //对cnt_32计数，每个计数周期对应一个字节或延时周期(640ns)
    reg [1:0]   cnt_sck;                        //对系统时钟四分频得到12.5M作为mosi的串行输出时钟
    wire        flag_cnt_en;                    //cnt_sck和cnt_bit计数有效的区间
    reg [2:0]   cnt_bit;                        //输出指令的串行比特
    reg [23:0]  addr_reg;                       //寄存地址，防止第一次写数据写入的地址错误
    reg [23:0]  addr_op;                        //实际写数据要写入的地址


    //------------------------------程序逻辑控制--------------------------------//

    //对输入数据进行计数,这与页大小有关
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            data_num <= 8'd0;
        else if(pi_flag == 1'b1)
            data_num <= data_num + 8'd1;
        else
            data_num <= data_num;
    end

    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_32 <= 5'd0;
        else if(cnt_32 == 5'd31)
            cnt_32 <= 5'd0;
        else if(state == IDLE)
            cnt_32 <= 5'd0;
        else
            cnt_32 <= cnt_32 + 5'd1;
    end

    assign  full_cnt_32_flag = (cnt_32 == 5'd31);

    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_32_ctrl <= 8'd0;
        else if((cnt_32_ctrl == NUM_CNT_32)&&(full_cnt_32_flag == 1'b1))
            cnt_32_ctrl <= 8'd0;
        else if(full_cnt_32_flag == 1'b1)
            cnt_32_ctrl <= cnt_32_ctrl + 8'd1;
        else
            cnt_32_ctrl <= cnt_32_ctrl;
    end


    //状态机状态控制逻辑
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0) 
            state <= IDLE;
        else begin 
            case(state)
                IDLE  : state <= (pi_flag == 1'b1) ? WR_EN : state;
                WR_EN : state <= ((cnt_32_ctrl == 8'd2) && (full_cnt_32_flag == 1'b1)) ? DELAY : state;
                DELAY : state <= ((cnt_32_ctrl == 8'd3) && (full_cnt_32_flag == 1'b1)) ? WR_PP : state;
                WR_PP : state <= ((cnt_32_ctrl == NUM_CNT_32) && (full_cnt_32_flag == 1'b1)) ? IDLE : state;
                default : state <= IDLE;
            endcase
        end
    end

    //片选信号控制
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_cs_n <= 1'b1;                 //低有效，复位为高
        else if(pi_flag == 1'b1)
            o_cs_n <= 1'b0;
        else if((cnt_32_ctrl == 8'd2)&&(full_cnt_32_flag == 1'b1))
            o_cs_n <= 1'b1;
        else if((cnt_32_ctrl == 8'd3)&&(full_cnt_32_flag == 1'b1))
            o_cs_n <= 1'b0;
        else if((cnt_32_ctrl == NUM_CNT_32)&&(full_cnt_32_flag == 1'b1))
            o_cs_n <= 1'b1;
        else
            o_cs_n <= o_cs_n;
    end 

    assign  flag_cnt_en = (cnt_32_ctrl >= 7'd5)&&(cnt_32_ctrl <= NUM_CNT_32 - 1'b1);
    
    //sck信号的输出
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_sck <= 2'd0;
        else if(cnt_sck == 2'd3)
            cnt_sck <= 2'd0;
        else if(cnt_32_ctrl == 8'd1)
            cnt_sck <= cnt_sck + 2'd1;
        else if(flag_cnt_en == 1'b1)
            cnt_sck <= cnt_sck + 2'd1;
        else
            cnt_sck <= 2'd0;
    end

    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_sck <= 1'b0;
        else if(cnt_sck == 2'd0)
            o_sck <= 1'b0;
        else if(cnt_sck == 2'd2)
            o_sck <= 1'b1;
        else
            o_sck <= o_sck;
    end

    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_bit <= 3'd0;
        else if((cnt_bit == 3'd7)&&(cnt_sck == 2'd2))
            cnt_bit <= 3'd0;
        else if((cnt_32_ctrl == 8'd1)&&(cnt_sck == 2'd2))
            cnt_bit <= cnt_bit + 3'd1;
        else if((flag_cnt_en == 1'b1)&&(cnt_sck == 2'd2))
            cnt_bit <= cnt_bit + 3'd1;
        else
            cnt_bit <= cnt_bit;
    end 

    //写入地址寄存
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            addr_reg <= INIT_ADDR;
        else if(pi_flag == 1'b1)
            addr_reg <= addr_reg + 1'b1;
        else
            addr_reg <= addr_reg;
    end

    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            addr_op <= 24'd0;
        else if(pi_flag == 1'b1)
            addr_op <= addr_reg;
        else
            addr_op <= addr_op;
    end

    //输出mosi信号
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_mosi <= 1'b0;
        else if((state == IDLE)||(cnt_32_ctrl >= NUM_CNT_32))
            o_mosi <= 1'b0;
        else if((state == WR_EN)&&(cnt_32_ctrl == 8'd1)&&(cnt_sck == 2'd0))
            o_mosi <= WEL[7-cnt_bit];                   //写使能指令
        else if((state == WR_PP)&&(cnt_32_ctrl == 8'd5)&&(cnt_sck == 2'd0))
            o_mosi <= PP[7-cnt_bit];                    //页写指令
        else if((state == WR_PP)&&(cnt_32_ctrl == 8'd6)&&(cnt_sck == 2'd0))
            o_mosi <= addr_op[23-cnt_bit];              //扇区地址
        else if((state == WR_PP)&&(cnt_32_ctrl == 8'd7)&&(cnt_sck == 2'd0))
            o_mosi <= addr_op[15-cnt_bit];              //页地址
        else if((state == WR_PP)&&(cnt_32_ctrl == 8'd8)&&(cnt_sck == 2'd0))
            o_mosi <= addr_op[7-cnt_bit];               //字节数据地址
        else if((state == WR_PP)&&(cnt_32_ctrl == 8'd9)&&(cnt_sck == 2'd0))
            o_mosi <= pi_data[7-cnt_bit];               //写入的数据
        else
            o_mosi <= o_mosi;
    end
    
endmodule