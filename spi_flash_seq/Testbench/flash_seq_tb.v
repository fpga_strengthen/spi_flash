`timescale 1ns/1ps

//关于发送完1bit后延时时间的说明，发送完一帧数据后延时一个Baud的时间
//RB是波特率，为9600Baud；Rb是比特速率，Rb=RB*logM，由于二进制表示，所以M=2,RB=Rb
//而RB=1/TB=9600,所以TB=1/9600s
//它所对应的50M系统时钟的周期数为TB/Ts=(1/9600)/(1/50_000_000)≈5208，即要延时时钟周期数为(5208*20)
//为了缩短仿真时间，将系统时钟和延时同步缩小100倍
//因此系统时钟为5_00_000Hz，即0.5MHz；延时时间为(5208 * 0.2) ≈ 1040

module flash_seq_tb();

    reg     clk;                //50M系统时钟
    reg     rst_n;              //复位，低有效
    reg     rx;

    wire    sck;
    wire    cs_n;
    wire    mosi;
    wire    tx_bit;

    reg [7:0]   data_mem [299:0];

    initial begin
        clk = 1'b0;
        rst_n <= 1'b0;
        #100
        rst_n <= 1'b1;
    end

    always #10 clk = ~clk;          //50M系统时钟

    //重定义仿真时间参数，缩短仿真时间
    defparam    U_flash_seq_top.CLK_FREQ = 500_000;

    initial begin 
        //读取输入的数据比特文件
        $readmemh("spi_flash.txt", data_mem);
    end

    //发送比特
    initial begin 
        rx <= 1'b0;                 //发送初始化
        #200
        rx_byte();
    end

    //定义任务：字节发送
    task rx_byte();
        integer j;
        for(j=0;j<300;j=j+1)
            rx_bit(data_mem[j]);
    endtask

    //该任务按照RS232协议模拟将接收的数据字节转换为串行比特数据
    task rx_bit(
        input   [7:0]   data
    );
        integer i;
        for(i=0;i<10;i=i+1)begin 
            case(i)
                0 : rx <= 1'b0;             //起始位
                1 : rx <= data[0];          //该字节的第一个bit，发送顺序从LSB~MSB
                2 : rx <= data[1];
                3 : rx <= data[2];
                4 : rx <= data[3];
                5 : rx <= data[4];
                6 : rx <= data[5];
                7 : rx <= data[6];
                8 : rx <= data[7];          //0~7为数据
                9 : rx <= 1'b1;             //停止位
                default : rx <= 1'b0;   //默认发送低电平
            endcase
            #1040;                      //每发送 1 位数据延时一个波特的时间
        end
    endtask

    spi_flash_seq_top U_flash_seq_top(
        .i_clk_50M  (clk),
        .i_rst_n    (rst_n),
        .i_data_bit (rx),

        .o_cs_n     (cs_n),
        .o_mosi     (mosi),
        .o_sck      (sck),
        .o_data_bit (tx_bit)
    );

endmodule