module spi_flash_pp_top(
    i_clk_50M,
    i_rst_n,
    i_key_touch,
    o_cs_n,
    o_sck,
    o_mosi
);
    input   wire    i_clk_50M;          //系统时钟，50MHz
    input   wire    i_rst_n;            //复位，低有效
    input   wire    i_key_touch;        //输入按键信号，是一个窄脉冲

    output  wire    o_cs_n;             //片选信号，低有效
    output  wire    o_mosi;             //输出串行输出信号
    output  wire    o_sck;              //mosi的时钟，在上升沿对mosi进行采样，在下降沿对mosi更新

    wire    flag_key;                   //按键消抖后的稳定按键脉冲信号

    parameter   CNT_MAX_KEY = 20'd999_999;

    key_filter
    #(
        .CNT_MAX(CNT_MAX_KEY)           //计数器计数最大值
    )
    U_key_filter
    (
        .sys_clk    (i_clk_50M),        //系统时钟50Mhz
        .sys_rst_n  (i_rst_n),          //全局复位
        .key_in     (i_key_touch),      //按键输入信号

        .key_flag   (flag_key)          //key_flag为1时表示消抖后检测到按键被按下,key_flag为0时表示没有检测到按键被按下                              
    );

    flash_pp_ctrl U_flash_pp(
        .i_clk_50M  (i_clk_50M),
        .i_rst_n    (i_rst_n),
        .i_key      (flag_key),
        .o_cs_n     (o_cs_n),
        .o_mosi     (o_mosi),
        .o_sck      (o_sck)
    );

endmodule