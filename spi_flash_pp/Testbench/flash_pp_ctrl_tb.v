`timescale 1ns/1ps
module flash_pp_ctrl_tb();
    
    reg clk_50M;
    reg rst_n;
    reg key;

    wire    cs_n;
    wire    mosi;
    wire    sck;

    initial begin
        clk_50M = 1'b0;
        rst_n <= 1'b0;
        #100
        rst_n <= 1'b1;
        key <= 1'b1;
        #20
        key <= 1'b0;
    end

    always #10 clk_50M = ~clk_50M;

    flash_pp_ctrl U_flash_pp_ctrl(
        .i_clk_50M  (clk_50M),
        .i_rst_n    (rst_n),
        .i_key      (key),
        .o_cs_n     (cs_n),
        .o_mosi     (mosi),
        .o_sck      (sck)
    );
endmodule