//----------------------------------------------------------------//
//功能；利用SPI（模式0）实现对Flash扇区0的数据擦除
//操作流程
// Items        |--------------------------------------------------------------------------------------------------|------------
// state        |   IDLE     |       WR_EN         |  DELAY  |                    WR_SE                   |        |     IDLE
//cnt_32_ctrl   |     0             |   1   |   2  |     3   |    4    |   5   |   6    |    7   |    8   |   9    |     ...
//instrument    |   NULL     | NULL | WR_EN | NULL |   NULL  |   NULL  |SE_inst|SE_Byte1|SE_Byte2|SE_Byte3|  NULL  |     ...
// cnt_bit      |     0             |  0~7  |              0           |  0~7  |   0~7  |   0~7  |   0~7  |   0    |     ...   
//EndOfItems    |--------------------------------------------------------------------------------------------------|------------
//----------------------------------------------------------------\\
module spi_flash_se_ctrl(
    i_clk_50M,
    i_rst_n,
    i_key,
    o_cs_n,
    o_sck,
    o_mosi
);
    input   wire    i_clk_50M;
    input   wire    i_rst_n;
    input   wire    i_key;
    
    output  reg     o_cs_n;
    output  reg     o_sck;
    output  reg     o_mosi;


    //待写入的指令
    parameter   WR_LOCK = 8'b0000_0110,                 //写使能指令
                SE      = 8'b1101_1000,                 //扇区擦除指令
                SADDR   = 24'h00_04_25;                 //扇区0地址

    parameter   IDLE  = 4'b0001,                    //空闲状态
                WR_EN = 4'b0010,                    //写使能状态，对Flash进行锁存
                DELAY = 4'b01000,                   //指令之间的延时（此时cs为高）
                WR_SE = 4'b1000;                    //扇区擦除状态

    reg [3:0]   state;          //状态机状态变量，用One-hot编码表示
    
    reg [4:0]   cnt_32;         //计数32个系统时钟，32*20=640ns
    
    //计数640ns，只在总线非空闲状态时做计时
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_32 <= 5'd0;
        else if(cnt_32 == 5'd31)
            cnt_32 <= 5'd0;
        else if(state != IDLE)
            cnt_32 <= cnt_32 + 5'd1;
        else
            cnt_32 <= cnt_32;
    end

    reg [3:0]   cnt_32_ctrl;            //对cnt_32计数以识别当前总线状态1

    //对cnt_32计数
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_32_ctrl <= 4'd0;
        else if((cnt_32_ctrl == 4'd9) && (cnt_32 == 5'd31))
            cnt_32_ctrl <= 4'd0;                //整个扇区擦除指令全部写入完毕后不再计数
        else if(cnt_32 == 5'd31)
            cnt_32_ctrl <= cnt_32_ctrl + 4'd1;
        else
            cnt_32_ctrl <= cnt_32_ctrl;
    end

    wire    full_cnt32_flag;        //cnt_32每计满31时就拉高该信号

    assign  full_cnt32_flag = (cnt_32 == 5'd31);

    //状态机状态跳转逻辑
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            state <= 4'b0000;
        else begin 
            case(state)
                IDLE : begin 
                    if(i_key == 1'b1)
                        state <= WR_EN;
                    else 
                        state <= state;
                end
                WR_EN : begin 
                    if((cnt_32_ctrl == 4'd2)&&(full_cnt32_flag == 1'b1))
                        state <= DELAY;
                    else
                        state <= state;
                end
                DELAY : begin 
                    if((cnt_32_ctrl == 4'd3)&&(full_cnt32_flag == 1'b1))
                        state <= WR_SE;
                    else
                        state <= state;
                end
                WR_SE : begin 
                    if((cnt_32_ctrl == 4'd9)&&(full_cnt32_flag == 1'b1))
                        state <= IDLE;
                    else
                        state <= state;
                end
                default:state <= IDLE;
            endcase
        end
    end

    //片选信号控制
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_cs_n <= 1'b1;         //低有效，默认为高
        else if(i_key == 1'b1)
            o_cs_n <= 1'b0;
        else if((cnt_32_ctrl == 4'd2)&&(full_cnt32_flag == 1'b1))
            o_cs_n <= 1'b1;
        else if((cnt_32_ctrl == 4'd3)&&(full_cnt32_flag == 1'b1))
            o_cs_n <= 1'b0;
        else if((cnt_32_ctrl == 4'd9)&&(full_cnt32_flag == 1'b1))
            o_cs_n <= 1'b1;
        else
            o_cs_n <= o_cs_n;
    end

    //在有效的写命令区间内对写入的比特进行计数

    
    //计数0~3，对系统时钟四分频得到12.5M作为sck信号的频率
    reg [1:0]   cnt_clk;
    wire        WR_SE_en;
    assign      WR_SE_en = (cnt_32_ctrl >= 4'd5)&&(cnt_32_ctrl <= 4'd8);
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_clk <= 2'd0;
        else if(cnt_clk == 2'd3)
            cnt_clk <= 2'd0;
        else if((cnt_32_ctrl == 4'd2) || (WR_SE_en == 1'b1))
            cnt_clk <= cnt_clk + 2'd1;
        else
            cnt_clk <= 2'd0;
    end

    
    //生成输出数据控制时钟信号sck，12.5M
    always @(posedge i_clk_50M or negedge i_rst_n) begin
        if(i_rst_n == 1'b0)
            o_sck <= 1'b0;              //模式0条件下，sck空闲状态为低电平
        else if((cnt_clk == 2'd0)&&(cnt_32_ctrl == 4'd2))
            o_sck <= 1'b1;
        else if((cnt_clk == 2'd2)&&(cnt_32_ctrl == 4'd2))
            o_sck <= 1'b0;
        else if((cnt_clk == 2'd0)&&(WR_SE_en == 1'b1))
            o_sck <= 1'b1;
        else if((cnt_clk == 2'd2)&&(WR_SE_en == 1'b1))
            o_sck <= 1'b0;
        else if((state == IDLE)||(state == DELAY))
            o_sck <= 1'b0;
        else
            o_sck <= o_sck;
    end



endmodule